<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 23.11.2018
 * Time: 13:18
 */

namespace OptionManager\Repository;


use Azizyus\LaravelLanguageHelper\App\Models\ILanguage;
use OptionManager\Models\Option;

class OptionRepository
{


    public function baseQuery()
    {
        return Option::query();
    }

    public function findById($id)
    {
        return $this->baseQuery()->where("id",$id)->first();
    }

    public function getByKey($key)
    {
        return $this->baseQuery()->where("key",$key)->first();
    }

    public function getAll()
    {
        return $this->baseQuery()->get();
    }

    protected $all = null;
    public function getAllSingle()
    {

        if($this->all == null)
        {
            $this->all = $this->getAll();
        }

        return $this->all;

    }

    public function updateOrInsert($key,$value,$group=0,ILanguage $language=null)
    {

        $option = $this->baseQuery()->where("key",$key);

        if($language!=null)
                $option = $option->where('language',$language->getId());

        $option = $option->first();

        if(!$option)
        {
            $option = new Option();
            $option->key = $key;
            $option->group = $group;
            $option->language = $language != null ? $language->getId() : null;
        }

        $option->value = $value;
        $option->save();


    }

    public function getGroup($group)
    {

        return $this->baseQuery()->where("group",$group)->get();

    }

}
