<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 2/28/19
 * Time: 2:02 PM
 */

namespace OptionManager\OptionManager;


use Illuminate\Http\Request;
use LaravelUploadHelper\NamePolicies\RandomNamePolicy;
use LaravelUploadHelper\UploadHelper\UploadedFileCatcher;
use LaravelUploadHelper\UploadHelper\UploadHelper;
use LaravelUploadHelper\UploadHelper\UploadHelperDeleter;
use LaravelUploadHelperImageTreatmentImplementations\FileTreatments\SmartPermeableImageTreatment;

class UploadFileHacker
{


    /**
     * @param Request $request
     * @param         $group
     *
     * @return Request
     *
     */

    // catch all files from request
    //find their input names (which is already should be same with option files)
    //upload file and save
    //replace the uploaded file with uploaded file name in request

    //$request->yourUploadedFile (really file)
    //$request->yourUploadedFile = $yourNewlySaveFileName
    //you cant replace on old request so you need new instance
    //thats what exactly what am doing here


    // you are tricking the request to read newly uploaded file name

    public function process(Request $request,array $groupData) : Request
    {

        $inputNames = array_keys($request->files->all());
        $newInputNames = [];

        foreach ($inputNames as $inputName)
        {


            if($request->exists("$inputName"))
            {



                $file = UploadedFileCatcher::catchFile($inputName);
                $uploadHelper = new UploadHelper($file);
                $uploadHelper->setNamingPolicy(new RandomNamePolicy());
                $uploadHelper->setFileTreatment(new SmartPermeableImageTreatment());
                $uploadedFileName = $uploadHelper->saveFile();
                $newInputNames[$inputName] = $uploadedFileName;

                $oldFileName = $groupData[$inputName];
                $uploadFileDeleter = new UploadHelperDeleter();
                $uploadFileDeleter->delete($oldFileName);

            }
            else
            {
                $newInputNames[$inputName] = $groupData[$inputName];

            }
        }


        $newRequestData = $request->except($inputNames);
        $newRequestData = array_merge($newRequestData,$newInputNames);

        $newRequest = new Request();
        $newRequest->replace($newRequestData);

        return $newRequest;

    }

}