<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 21.12.2018
 * Time: 17:08
 */

namespace OptionManager\OptionManager;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;

class ConfigLoader
{


    /**
     * @return \Illuminate\Config\Repository|mixed
     *
     * it returns your main config array @see option_manager.php
     */
    public function readConfigManagerFile()
    {
        return config("option_manager");
    }


    /**
     * @param $options
     *
     * $options param is comes from OptionManager ->load();
     *
     *
     */
    public function load($options)
    {



        $configs = $this->readConfigManagerFile();
        foreach ($configs as $configData)
        {

            $fileData  = $configData;

            $fileName = $fileData["fileName"];
            $group = $fileData["group"];



            $configParams = config($fileName);


            foreach ($configParams as $key => $configParam)
            {


                $option = $options->where("group",$group)->where("key",$key)->first();

                if($option)
                {
                    Config::set($fileName.".{$option->key}",$option->value);
                }
            }

        }


    }

    /**
     * @return array
     *
     * you should return array which is combines all of your config files as
     * [key => value]
     * and key is config('yourFile.KEY')
     * and your value is what it returns
     *
     */
    public function vomit() : array
    {


        $configs = $this->readConfigManagerFile();
        $readyToShareOptions=[];
        foreach ($configs as $configData) {

            $fileData = $configData;

            $fileName = $fileData["fileName"];
            $group = $fileData["group"];


            $configParams = config($fileName);


            foreach ($configParams as $key => $configParam) {

                $readyToShareOptions[$key] = $configParam;

            }
        }

        return $readyToShareOptions;


    }


    /**
     * @param $vomit
     * share to blade view your $this->vomit();
     */
    public function share($vomit)
    {

        View::share($vomit);

    }

}