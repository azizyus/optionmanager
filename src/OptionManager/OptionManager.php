<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 21.12.2018
 * Time: 17:07
 */

namespace OptionManager\OptionManager;


use Azizyus\LaravelLanguageHelper\App\Models\ILanguage;
use Illuminate\Http\Request;
use OptionManager\Models\Option;
use OptionManager\Repository\OptionRepository;

class OptionManager
{


    protected $optionRepository;
    protected $configLoader;
    protected $uploadFileHacker;

    private $options = null;

    public function __construct()
    {

        $this->optionRepository = new OptionRepository();
        $this->configLoader = new ConfigLoader();
        $this->uploadFileHacker = new UploadFileHacker();
    }

    public function initLanguageScope(\Closure $c) : void
    {
        Option::addGlobalScope('language',function($q)use($c){
            $q->where('language',null)->orWhere('language',call_user_func($c));
        });
    }

    public function clearLanguageScope() : void
    {
        Option::addGlobalScope('language',function($q){});
    }

    public function setUploadFileHack(UploadFileHacker $uploadFileHacker)
    {
        $this->uploadFileHacker = $uploadFileHacker;
    }

    public function setConfigLoader(ConfigLoader $configLoader)
    {
        $this->configLoader = $configLoader;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|null
     *
     * load options once
     */
    protected function getAllOptions()
    {
        if(!$this->options) $this->options = $this->optionRepository->getAll();
        return $this->options;
    }

    public function fetchOption($key,$group,ILanguage $language=null)
    {
        return $this->fetchOptionByLanguageId($key,$group,$language!=null ? $language->getId() : null);
    }

    protected function gatherOption($key,$group,$languageId=null)
    {
        $option = $this->getAllOptions()
            ->where('key',$key)
            ->where('group',$group);

        if($languageId)
            $option = $option->where('language',$languageId);

        return $option->first();
    }

    public function fetchOptionByLanguageId($key,$group,$languageId=null)
    {
        $option = $this->gatherOption($key,$group,$languageId);
        return $option!=null ? $option->value : null;
    }

    public function load()
    {
        $this->configLoader->load($this->getAllOptions());
    }

    protected function vomit()
    {
        return $this->configLoader->vomit();
    }

    public function share()
    {
        $this->configLoader->share($this->vomit());
    }

    protected function tryGetConfigByGroup($group)
    {

        foreach ($this->configLoader->readConfigManagerFile() as $option)
        {

            if($option["group"] == $group)
            {
                return config($option["fileName"]);
            }

        }

        throw new \Exception("I CANT FIND YOUR GROUP ($group)");

    }

    public function saveOption($key,$data,Int $group=0,ILanguage $language = null)
    {
        $this->optionRepository->updateOrInsert($key,$data,$group,$language);
    }

    public function deleteOption($key,$group,ILanguage $language=null)
    {
        $option = $this->gatherOption($key,$group,$language);
        if($option)
            $option->delete();
        return $option;
    }

    public function saveKeysAndLanguage(array $keys,Request $request,$group,$language)
    {
        foreach ($keys as $key)
        {
            $data = $request->get($key.$language->id);
            $this->saveOption($key,$data,$group,$language);
        }
    }

    public function saveFileKeysAndLanguage(array $keys,Request $request,$group,$language)
    {
        $request = $this->uploadFileHacker->process($request, $request->allFiles());
        foreach ($keys as $key)
        {
            $data = $request->get($key.$language->id);
            $this->saveOption($key,$data,$group,$language);
        }
    }

    public function saveFileKeys(array $keys,Request $request,$group,$language)
    {
        $request = $this->uploadFileHacker->process($request, $request->allFiles());
        $this->saveKeys($keys,$request,$group,$language);
    }

    public function saveKeys(array $keys,Request $request,$group,$language)
    {
        foreach ($keys as $key)
        {
            $data = $request->get($key);
            $this->saveOption($key,$data,$group,$language);
        }
    }


    public function save(Request $request,$group=0)
    {
        $request = $this->uploadFileHacker->process($request,$request->all());

        foreach ($request->all() as $key => $value)
        {
            $this->saveOption($key,$value,$group);
        }

    }

}
