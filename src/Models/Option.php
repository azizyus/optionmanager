<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 23.11.2018
 * Time: 12:59
 */

namespace OptionManager\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Option
 * @package OptionManager\Models
 *
 * @property String $key
 * @property String $value
 * @property String $valueData
 * @property String $group
 * @property String $language
 *
 */

class Option extends Model
{

    protected $table = "options";

    protected $casts = [
        'valueData' => 'array'
    ];

    protected $fillable = ["key","value","valueData","group","language"];

    public function setValueAttribute($v)
    {
        if(is_array($v))
            $this->valueData = $v;
        else
            $this->attributes['value'] = $v;
    }

    public function getValueAttribute()
    {
        if($this->valueData)
            return $this->valueData;
        return $this->attributes['value'];
    }

}
