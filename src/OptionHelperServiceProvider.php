<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 23.11.2018
 * Time: 12:54
 */

namespace OptionManager;
use Illuminate\Support\ServiceProvider;

class OptionHelperServiceProvider extends ServiceProvider
{

    public function boot()
    {

        $this->loadMigrationsFrom(__DIR__."/Migrations");

        $this->publishes([
            __DIR__."/Config/option_manager.php"=>config_path("option_manager.php")
        ],
        "azizyus/optionmanger-config");




    }


    public function register()
    {



    }

}