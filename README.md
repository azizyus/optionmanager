# Option Manager

This package overrides your config files from database(or where ever you want, see ConfigLoader.php).

## Getting Started

lets think you want to create options page for you site, you need to define Site Name, 
maybe contact email for you contact form, what you need to do? creating config file will be fine too but 
you cant just put these email address to git, so this thing helps you to put these things to database.
<br>


### Prerequisites

Laravel 5.* <br>
php >=7.1

### Installing

php artisan migrate <br>
php artisan vendor:publish azizyus/optionmanger-config <br>


### LOGIC AND EXAMPLE

check example dir while reading this part <br>
you have a controller which is serves your config page <br>
also that controller catches your post request to save your new options <br>
you save it in panel (YourPanelOptionsController::saveOptions()) <br>
then read from db, override your config file then spread with View::share(); (check YourContextOrFrontController::__construct()) <br>
basically you should able to use your {{$name}} instead of config('yourConfigFile.name')



