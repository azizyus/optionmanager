<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 21.12.2018
 * Time: 22:55
 */

class YourContextOrFrontController extends \App\Http\Controllers\Controller
{


    public function __construct()
    {


        $optionManager = new \OptionManager\OptionManager\OptionManager();

        $optionManager->load(); //you readed from db and overrided your config files which is your already provided in option_manager


        $optionManager->share(); //View::share(); to reach your options

    }

}