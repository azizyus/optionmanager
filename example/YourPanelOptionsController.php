<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use OptionManager\OptionManager\OptionManager;
use OptionManager\Repository\OptionRepository;

class YourPanelOptionsController extends Controller
{

    public $optionManager;
    public function __construct()
    {

        $this->optionManager = new OptionManager();
    }



    public function siteOptions()
    {


        $options = config()->get("yourConfigFile");

        return view("yourOptionsPage.blade")->with($options);

    }

    public function saveOptions(Request $request)
    {

        $group = $request->get("group");
        $this->optionManager->save($request,$group);

        return back();

    }
}
