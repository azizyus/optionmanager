@extends("admin.layouts.app")


@section("content")




    <div class="card">

        <form method="POST" action="{{route("any_post_route_goes_to_---YourPanelOptionsController::saveOptions()")}}" class="card-body">
            {{csrf_field()}}

            <input type="hidden" name="group" value="USE_THE_ENUM_YOU_PROVIDED_IN_OPTION_MANAGER_FILE">
            <div class="row">


                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="front-currency">Forced Https</label>
                        <select name="forcedHttps" id="forcedHttps" class="form-control">
                            <option @if($forcedHttps == true) selected="selected" @endif value="1">Evet</option>
                            <option @if($forcedHttps == false) selected="selected" @endif value="0">Hayır</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="site-phone">Phone Number</label>
                        <input value="{{$sitePhone}}" type="text" name="sitePhone" id="site-phone" class="form-control">
                    </div>
                </div>


                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="contact-email">Contact Email</label>
                        <input value="{{$contactEmail}}" type="email" name="contactEmail" id="contact-email" class="form-control" required>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="reservation-email">Reservation Email</label>
                        <input value="{{$reservationEmail}}" type="email" name="reservationEmail" id="reservation-email" class="form-control" required>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="info-email">Info Email</label>
                        <input value="{{$infoEmail}}" type="email" name="infoEmail" id="info-email" class="form-control" required>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="error-email">Error Email</label>
                        <input value="{{$errorEmail}}" type="email" name="errorEmail" id="error-email" class="form-control" required>
                    </div>
                </div>

                <div class="col-lg-12">

                    <input type="submit" value="Update" name="update"  class="pull-right btn btn-md btn-primary">

                </div>

            </div>



        </form>

    </div>

@endsection