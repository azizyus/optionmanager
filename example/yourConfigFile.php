<?php



return [



    "sitePhone" => null,
    "contactEmail" => null,
    "reservationEmail" => null,
    "infoEmail" => null,
    "phoneNumber" => null,

    "errorEmail" => null,

    "forcedHttps" => false,

];